.. code:: robotframework

   *** Settings ***
   Library                      OperatingSystem
   Library                      lib/LoginLibrary.py

   *** Variables ***
   ${MESSAGE}                   Hello, world!
   ${EXPECTED_FIBONACCI}        1 1 2 3 5 8 13 21 34 55 89 144

   *** Test Cases ***
   My Test
            [Documentation]         Example test
            Log                     ${MESSAGE}
            Fibonacci Test          12
            Fibonacci Should Be     ${EXPECTED_FIBONACCI}
    
    Another Test
        Should Be Equal         ${MESSAGE}    Hello, world!

    